#include <stdio.h>
float input();
float compute(float);
float output(float,float);
int main();
{
	float f, c; 
	f=input();
	c=compute(f);
	output(f,c);
	return 0;
}

float input()
{
	float f1;
	printf("enter the degrees in Fahrenheit");
	scanf("%f",&f1);
	return f1;
}

float compute(float f)
{
	return (f-32)*5/9;
}

float output(float f, float c)
{
	printf("the conversion of degrees Fahrenheit %f to Celsius in %f",f, c); 
}