#include <stdio.h>

int main()
{
    struct book 
    {
        char name[50];
        int number;
        char author[20];
        struct date
        {
            int dd,mm,yy;
        }doi,dor;
    }lb={"c++",12219,"reema thareja",20,11,2019,10,12,2019};
    printf("\nLibrary book details are : ");
    printf("\n Book name : %s\n Book number : %d\n Book author : %s\n Date of issue : %d-%d-%d\n Date of return : %d-%d-%d\n",lb.name,lb.number,lb.author,lb.doi.dd,lb.doi.mm,lb.doi.yy,lb.dor.dd,lb.dor.mm,lb.dor.yy);
    return 0;   
}